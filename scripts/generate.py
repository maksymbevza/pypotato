import random

import faiss
import numpy as np
import pymesh

from potato.curves import normalize
from potato.curves.dist import roll_all
from potato.curves.glc import glc
from potato.mod import scale, get_random_unit_vector


RADIUS = 1
DETAILS = 3

BEAM_SIZE = 160
DIM = 50


def get_lc(x):
    #return np.random.rand(DIM).astype(np.float32)
    t, l = glc(x, bins=DIM)
    return np.array(l)


def main(seed):
    start = [pymesh.generate_icosphere(RADIUS, [0, 0, 0], DETAILS)]

    db = faiss.IndexFlatL2(DIM)
    mesh_db = []

    x = pymesh.load_mesh("data/Mentor_3451.obj")
    LC = get_lc(x)
    LC = normalize(LC)

    try:
        batch_i = 0
        while db.ntotal < 300_000:
            batch_i += 1
            print(f"Batch #{batch_i}: {db.ntotal}")

            #lc = get_lc_batch(start, 6*4)  # 24
            new_lcs = []
            for x in start:
                lc = get_lc(x)
                lc = normalize(lc)
                new_lcs.extend(roll_all(lc))


            # FILTERING IS DISABLED
            # Filter by how much of the 24 orientations match
            # useful_indices = eliminate_occurences(db, lc)
            # lc = lc[useful_indeces]

            new_lcs = np.array(new_lcs, dtype=np.float32)
            if db.ntotal > 0:
                D, I = db.search(new_lcs, k=1)
                print("Mean dist: ", D.mean())

            LC = np.array(LC, dtype=np.float32).reshape((1, -1))
            nD, nI = db.search(LC, k=1)
            print("EVAL Mean dist: ", nD.mean())
            print("EVAL Idx dist: ", nI[0, 0])

            db.add(new_lcs)
            mesh_db.extend([(x.vertices, x.faces) for x in start])

            #if len(start) >= BEAM_SIZE // 4:
            #    dists = D.reshape(-1, DIM).sum(1)
            #    order = np.argsort(dists)[::-1]
            #    start = [start[i] for i in order[:BEAM_SIZE // 4]]
            start = morph_batch(start, gen=5)
            if len(start) >= BEAM_SIZE:
                start = random.choices(start, k=BEAM_SIZE)

        raise KeyboardInterrupt
    except KeyboardInterrupt:
        print("Save DB")
        faiss.write_index(db, f"data/index_{DIM}_{seed}.db")
        print("Saved DB")
        print("Save Meshes")
        import pickle
        with open(f"data/index_mesh_{DIM}_{seed}.db", "wb") as f:
            pickle.dump(mesh_db, f)
        print("Saved Meshes")

    #from potato.vis import visualize_pymesh
    #for s in start[:3]:
    #    visualize_pymesh(s)


def morph_batch(meshes, gen: int):
    new_meshes = []
    for mesh in meshes:
        for i in range(gen):
            try:
                idx = random.randint(0, mesh.vertices.shape[0] - 1)
                vec = get_random_unit_vector()
                new_vertices = scale.scale(
                    mesh.vertices,
                    idx,
                    vec,
                    k0=random.randint(5, 15)/10)
                new_mesh = pymesh.form_mesh(new_vertices, mesh.faces)
                new_meshes.append(new_mesh)
            except Exception as e:
                raise
    return new_meshes


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--seed", default=0, type=int)

    args = parser.parse_args()
    random.seed(42 + args.seed)
    np.random.seed(42 + args.seed)

    main(args.seed)
