from potato.curves import get_distance, normalize

lc1 = np.arange(10)
lc2 = np.arange(10) + 5

lc1 = normalize(lc1)
lc2 = normalize(lc2)

print(get_distance(lc1, lc2))
