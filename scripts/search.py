import random

import faiss
import numpy as np
import pymesh
import pickle

from potato.curves import normalize
from potato.curves.dist import roll_all
from potato.curves.glc import glc
from potato.mod import scale, get_random_unit_vector


random.seed(42)
np.random.seed(42)


BEAM_SIZE = 50
GEN = 1000
DIM = 50


def get_lc(x):
    #return np.random.rand(DIM).astype(np.float32)
    t, l = glc(x, bins=DIM)
    return np.array(l, dtype=np.float32)


def load_meshes(filename):
    with open(filename, "rb") as f:
        data = pickle.load(f)
    data = [pymesh.form_mesh(*x) for x in data]
    return data


def main():
    db = faiss.read_index("data/index.db")
    mesh_db = load_meshes("data/index_mesh.db")
    print("Loaded DBs")

    x = pymesh.load_mesh("data/Mentor_3451.obj")
    LC = get_lc(x)
    LC = normalize(LC)

    LC = np.array(LC, dtype=np.float32).reshape((1, -1))
    D, I = db.search(LC, k=1)
    print("Mean dist: ", D.mean())
    print("Idx dist: ", I[0, 0])
    idx = I[0, 0] // DIM
    shift = I[0, 0] % DIM

    for i in range(5):
        start = [mesh_db[idx]]

        new_lcs = []
        for s in start:
            lc = get_lc(s)
            lc = normalize(lc)
            new_lcs.extend(roll_all(lc))

        new_lcs = np.array(new_lcs, dtype=np.float32)
        if db.ntotal > 0:
            D, I = db.search(new_lcs, k=1)
            print("Mean dist: ", D.mean())

        LC = np.array(LC, dtype=np.float32).reshape((1, -1))
        nD, nI = db.search(LC, k=1)
        print("EVAL Mean dist: ", nD.mean())
        print("EVAL Idx dist: ", nI[0, 0])

        db.add(new_lcs)
        #mesh_db.extend([(x.vertices, x.faces) for x in start])
        mesh_db.extend(start)

        #if len(start) >= BEAM_SIZE // 4:
        #    dists = D.reshape(-1, DIM).sum(1)
        #    order = np.argsort(dists)[::-1]
        #    start = [start[i] for i in order[:BEAM_SIZE // 4]]

        start = morph_batch(start, gen=GEN, depth=1)
        if len(start) >= BEAM_SIZE:
            start = random.choices(start, k=BEAM_SIZE)

    LC = np.array(LC, dtype=np.float32).reshape((1, -1))
    D, I = db.search(LC, k=1)
    print("EVAL Mean dist: ", D.mean())
    print("EVAL Idx dist: ", I[0, 0])
    idx = I[0, 0] // DIM
    shift = I[0, 0] % DIM


    from potato.vis import visualize_pymesh
    visualize_pymesh(x)
    visualize_pymesh(mesh_db[idx])

    import matplotlib.pyplot as plt
    c1 = get_lc(x)
    c2 = get_lc(mesh_db[idx])
    c1 /= c1.sum()
    c2 /= c2.sum()
    plt.plot(c1)
    shift = DIM - shift
    plt.plot(np.concatenate([c2[shift:], c2[:shift]]))
    plt.show()



def morph_batch(meshes, gen: int, depth: int = 1):
    new_meshes = []
    for mesh in meshes:
        for i in range(gen):
            new_mesh = mesh
            for j in range(depth):
                idx = random.randint(0, mesh.vertices.shape[0] - 1)
                vec = get_random_unit_vector()
                new_vertices = scale.scale(
                    new_mesh.vertices,
                    idx,
                    vec,
                    k0=random.randint(3, 30)/10)
                new_mesh = pymesh.form_mesh(new_vertices, mesh.faces)
            new_meshes.append(new_mesh)
    return new_meshes


if __name__ == "__main__":
    main()

