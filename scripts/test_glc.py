import pymesh

from potato.curves import glc

#_, res = glc.glc(pymesh.generate_icosphere(2, [0, 0, 0], 3),
#              frame_num=30)

_, res = glc.glc(
    "data/ico3.stl",
    frame_num=50,
    ang=0.2)

print(res)
