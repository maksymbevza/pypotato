import random

import numpy as np
import pymesh

from potato.mod import scale, get_random_unit_vector
from potato.vis import visualize_pymesh


random.seed(42)
np.random.seed(42)

mesh = pymesh.generate_icosphere(2, [0, 0, 0], 4)

N = 20
for i in range(N):
    idx = random.randint(0, mesh.vertices.shape[0])
    vec = -get_random_unit_vector()
    new_vertices = scale.scale(mesh.vertices, idx, vec, k0=1)
    mesh = pymesh.form_mesh(new_vertices, mesh.faces)

filename = f"data/random-deform-{N}.ply"
pymesh.save_mesh(filename, mesh);
import open3d as o3d
mesh_o3d = o3d.io.read_triangle_mesh(filename)
mesh_o3d.compute_vertex_normals()
o3d.io.write_triangle_mesh(filename, mesh_o3d)

visualize_pymesh(mesh)

