import numpy as np

from .norm import assert_norm


def get_distance(a, b):
    assert_norm(a)
    assert_norm(b)

    mat = roll_all(a)
    return np.abs(mat - b).sum(axis=1).min()


def roll_all(vec):
    n = vec.shape[0]
    assert len(vec.shape) == 1
    mat = np.repeat(vec.reshape(1, -1), n, axis=0)
    return _custom_roll(mat, np.arange(n))


#### BELOW IS COPY_PASTE JUST YET

from numpy.lib.stride_tricks import as_strided
# NumPy solution:
def _custom_roll(arr, r_tup):
    m = np.asarray(r_tup)
    arr_roll = arr[:, [*range(arr.shape[1]),*range(arr.shape[1]-1)]].copy() #need `copy`
    #print(arr_roll)
    strd_0, strd_1 = arr_roll.strides
    #print(strd_0, strd_1)
    n = arr.shape[1]
    result = as_strided(arr_roll, (*arr.shape, n), (strd_0 ,strd_1, strd_1))

    return result[np.arange(arr.shape[0]), (n-m)%n]
