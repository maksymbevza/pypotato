import numpy as np


def normalize(x):
    return x / get_norm(x)


def get_norm(x):
    return x.sum()


def assert_norm(x):
    assert np.abs(get_norm(x) - 1) <= 1e-6
