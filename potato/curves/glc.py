import open3d as o3d
import open3d.visualization.rendering as rendering
import numpy as np
import matplotlib.pyplot as plt
import pymesh

from potato.utils import pymesh_to_o3d

import cv2


def getLightValue(img):
    gr = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gr_float = gr.astype(np.float)
    max_val = np.max(gr_float)
    if max_val < 0.000000001:
        return 0.0
    sm = cv2.sumElems(gr_float)/ max_val
    return sm[0]


def showCurve(t, val):
    fig, ax = plt.subplots()
    ax.plot(t, val)

    ax.set(xlabel='time (s)', ylabel='intensity',
           title='LightCurve')
    ax.grid()

    # fig.savefig("test.png")
    plt.show()


def glc(obj_file_or_mesh, window_size=(120,80), bins = 50):
    if isinstance(obj_file_or_mesh, str):
        mesh = o3d.io.read_triangle_mesh(obj_file_or_mesh)
    elif isinstance(obj_file_or_mesh, pymesh.Mesh):
        mesh = pymesh_to_o3d(obj_file_or_mesh)
        mesh.compute_vertex_normals(normalized=True)
        mesh.compute_triangle_normals(normalized=True)
    else:
        mesh = obj_file_or_mesh

    mesh.compute_vertex_normals(normalized=True)
    mesh.compute_triangle_normals(normalized=True)

    render = rendering.OffscreenRenderer(window_size[0], window_size[1])


    l_value = []
    t_value = []


    v = np.array([0.0, 0.0, 2*np.pi/float(bins)], dtype=np.float64)

    render.scene.scene.enable_indirect_light(False)
    render.scene.scene.enable_sun_light(False)

    green = rendering.Material()
    green.base_color = [1.0, 1.0, 1.0, 1.0]
    green.shader = "defaultLit"
    mesh.scale(2.0, center=mesh.get_center())
    for i in range(0, bins, 1):

        R = mesh.get_rotation_matrix_from_axis_angle(v)
        mesh.rotate(R, center=(0, 0, 0))

        render.scene.add_geometry("cyl1", mesh, green)
        # render.scene.add_geometry("sphere", sphere, yellow)
        # render.scene.add_geometry("box", box, grey)
        # render.scene.add_geometry("solid", solid, white)
        render.setup_camera(60.0, [0, 0, 0], [0, 10, 0], [0, 0, 1])

        render.scene.scene.set_sun_light([0.707, -0.1, -.707], [1.0, 1.0, 1.0],
                                         75000)
        render.scene.scene.enable_sun_light(True)
        render.scene.set_background([0, 0, 0, 1])
        # render.scene.show_axes(True)

        img = render.render_to_image()
        img = np.array(img)

        sm = getLightValue(img)
        t_value.append(i)
        l_value.append(sm)
        #print(sm)
        # cv2.imshow("dfdfd", img)
        # cv2.waitKey(1)
        render.scene.remove_geometry("cyl1")

    if sum(l_value) < .001:
        raise Exception(f"Zero vector {l_value}")
    return t_value, l_value


# def main():
#
#     #v = np.array([0,1,0], dtype=np.float)
#
#     mesh = o3d.io.read_triangle_mesh("/home/yaroslav/Downloads/10464_Asteroid_L3.123c72035d71-abea-4a34-9131-5e9eeeffadcb/random-deform-20.ply")
#     render = rendering.OffscreenRenderer(240, 160)
#     #render.set_clear_color([0,0,0,1])
#     yellow = rendering.Material()
#     yellow.base_color = [1.0, 0.75, 0.0, 1.0]
#     yellow.shader = "defaultLit"
#
#     green = rendering.Material()
#     green.base_color = [1.0, 1.0, 1.0, 1.0]
#     green.shader = "defaultLit"
#
#     grey = rendering.Material()
#     grey.base_color = [0.7, 0.7, 0.7, 1.0]
#     grey.shader = "defaultLit"
#
#     white = rendering.Material()
#     white.base_color = [0.0, 0.0, 0.0, 0.0]
#     white.shader = "defaultLit"
#
#     # cyl = o3d.geometry.TriangleMesh.create_cylinder(0.05, 3)
#     # cyl.compute_vertex_normals()
#     # cyl.translate([-2, 0, 1.5])
#     # sphere = o3d.geometry.TriangleMesh.create_sphere(0.2)
#     # sphere.compute_vertex_normals()
#     # sphere.translate([-2, 0, 3])
#     #
#     # box = o3d.geometry.TriangleMesh.create_box(2, 2, 1)
#     # box.compute_vertex_normals()
#     # box.translate([-1, -1, 0])
#     # solid = o3d.geometry.TriangleMesh.create_icosahedron(0.5)
#     # solid.compute_triangle_normals()
#     # solid.compute_vertex_normals()
#     # solid.translate([0, 0, 1.75])
#
#     teta = 1.000000
#     v = np.array([0.0, 0.0, 0.2], dtype=np.float64)
#
#     render.scene.scene.enable_indirect_light(False)
#     render.scene.scene.enable_sun_light(False)
#
#
#
#
#
#     for i in range(0, 500, 1):
#         v = v * teta
#         R = mesh.get_rotation_matrix_from_axis_angle(v)
#         mesh.rotate(R, center=(0, 0, 0))
#         render.scene.add_geometry("cyl1", mesh, green)
#         # render.scene.add_geometry("sphere", sphere, yellow)
#         # render.scene.add_geometry("box", box, grey)
#         # render.scene.add_geometry("solid", solid, white)
#         render.setup_camera(60.0, [0, 0, 0], [0, 10, 0], [0, 0, 1])
#
#
#
#
#         render.scene.scene.set_sun_light([0.707, -0.1, -.707], [1.0, 1.0, 1.0],
#                                          75000)
#         render.scene.scene.enable_sun_light(True)
#         render.scene.set_background([0,0,0,1])
#         #render.scene.show_axes(True)
#
#         img = render.render_to_image()
#         img = np.array(img)
#         cv2.imshow("dfdfd", img)
#         cv2.waitKey(1)
#         render.scene.remove_geometry("cyl1")
    #o3d.io.write_image("/home/yaroslav/Downloads/10464_Asteroid_L3.123c72035d71-abea-4a34-9131-5e9eeeffadcb/test1.png", img, 9)

    # render.setup_camera(60.0, [0, 0, 0], [0, 10, 0], [0, 0, 1])
    # img = render.render_to_image()
    # o3d.io.write_image("/home/yaroslav/Downloads/10464_Asteroid_L3.123c72035d71-abea-4a34-9131-5e9eeeffadcb/test2.png", img, 9)


if __name__ == "__main__":
    #main()
    t, l = glc('/home/yaroslav/Downloads/10464_Asteroid_L3.123c72035d71-abea-4a34-9131-5e9eeeffadcb/random-deform-20.ply', bins=30)
    showCurve(t, l)
