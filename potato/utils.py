import tempfile

import numpy as np
import pymesh
import open3d as o3d


def get_gaussian(r: float, sigma: float) -> float:
    """"""
    return (2 * np.pi * sigma**2) ** -0.5 * np.exp(-0.5 * (r / sigma)**2)


def calc_dist(x, a):
    """Calculate distance every point x to base point a. """

    assert x.shape[1:] == a.shape == (3,)
    return ((x-a)**2).sum(axis=1)


def pymesh_to_o3d(mesh):
    with tempfile.NamedTemporaryFile("w+b", suffix=".ply") as f:
        filename = f.name
        pymesh.save_mesh(filename, mesh)
        return o3d.io.read_triangle_mesh(filename)
