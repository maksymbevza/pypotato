import numpy as np


def get_random_unit_vector():
    vec = np.random.random([3]) * 2 - 1
    vec /= (vec ** 2).sum() ** 0.5
    return vec
