import numpy as np

from potato.utils import get_gaussian, calc_dist


def get_gaussian_scales(x, a, sigma):
    dists = calc_dist(x, a)
    k = np.array([get_gaussian(d, sigma) for d in dists])
    return k


def scale(v, idx, vec, k0, sigma=1):
    k = k0 * get_gaussian_scales(v, v[idx], sigma=sigma)
    v = v + k.reshape(-1, 1) * vec
    return v
