import pymesh
import open3d as o3d

from potato.utils import pymesh_to_o3d


def visualize_pymesh(mesh):
    """Visualize pymesh mesh object. """

    mesh_o3d = pymesh_to_o3d(mesh)
    return visualize_o3d(mesh_o3d)


def visualize_o3d(mesh):
    """Visualize open3d mesh object. """

    mesh.compute_vertex_normals()
    o3d.visualization.draw_geometries([mesh])
