## Visualizations

### Example:
```python
import pymesh

from potato.vis import visualize_pymesh

mesh = pymesh.generate_icosphere(2, [0, 0, 10], 3)
visualize_pymesh(mesh)
```
