# PyPotato

![mentor-comparison](img/compare2.gif)

## About

- [Presentation](https://docs.google.com/presentation/d/1gguyK7WwLpauGvQjWUUCSL4Lvvf2DeDWTG4Ocq0tfuw/edit)
- [30-seconds video](https://www.youtube.com/watch?v=z6maqSK4KMU)
- [GDrive folder with shapes and light curves videos](https://drive.google.com/drive/folders/1yiOveSx3z9m2W8Xl1c9cN_SXgrq9dToK)

## Installation:
1. Install required libraries
2. Run `python setup.py develop`

## Requires:
1. open3d
2. pymesh (build from source)
